<?php
	session_start();

	if(isset($_SESSION['ADMIN_AUTHENTICATED'])) {
		header('Location: home.php');
		exit();
	}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>movies Quiz</title>

		
		<link href="css/bootstrap.css" rel="stylesheet">
		
    </head>
	
    <body>
	
			<div class="text-center header">
			</div>

			<div class="container-fluid">
			
			<div class="row ">
				<div class="col-md-12 ">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 jumbotron ">
							<form role="form" action="validate_login.php" method="POST">
								<div class="form-group ">
									 
									<label for="user_name_entry">
										User Name
									</label>
									<input type="text" class="form-control " name="user_name" id="user_name_entry">
								</div>
								<div class="form-group">
									 
									<label for="password_entry">
										Password
									</label>
									<input type="password" class="form-control" name="password" id="password_entry">
								</div>
								

								<button type="submit" class="btn btn-default pull-right">
									LOGIN
								</button>
							</form>
						</div>
						<div class="col-md-4">
						</div>
					</div>
				</div>
			</div>
		</div>
		
    </body>
</html>