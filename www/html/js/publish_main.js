
function submit_package() {
	console.log("submit_package");

	var clips_number = document.getElementById("clips_number_entry").value;
	var coins = document.getElementById("coins_entry").value;
	var color = document.getElementById("color_entry").value;

	if(clips_number == "" || coins == "" || color == "") {
		console.log("entry missing");
		return;
	}

	var entries = {
		clips : clips_number,
		coins : coins,
		color : color
	}
	var json = JSON.stringify(entries);

	$.ajax({
		type: "POST",
		url: "/publish/add_package.php",
		data: "entries="+json,
		cache: false,
		success: function(respond) {
			console.log(respond);
			if(respond == "done") {

				document.getElementById("clips_number_entry").value = "";
				document.getElementById("coins_entry").value = "";
				document.getElementById("color_entry").value = "";

				update_packages_list();
				update_clips();

			} else {
				alert("server responded with error");
			}
		}
	});
}

function update_packages_list() {
	console.log("update_packages_list");
}

function update_clips() {
	console.log("update_clips");
}

function publish_package(id) {
	console.log("publish_package");
}