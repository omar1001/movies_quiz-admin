<?php
	session_start();
	if(!isset($_SESSION['ADMIN_AUTHENTICATED']) || !isset($_SESSION['publisher'])) {
		header('Location: ../home.php');
		exit();
	}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>movies Quiz</title>

		<link href="../css/bootstrap.css" rel="stylesheet">
		<link href="../css/select2.min.css" rel="stylesheet">

		<script src="../js/jquery.min.js"></script>
		<script src="../js/select2.min.js"></script>

		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/publish_main.js"></script>
		
		
    </head>
	
    <body >

	<div class="container-fluid">
		<div class="row" style="padding:25px;">
			<div class="col-md-4">
				<h3 class="text-center text-primary">
					Package
				</h3>
				<form role="form">
					<div class="form-group">
						 
						<label for="clips_number_entry">
							Clips Number
						</label>
						<input type="text" class="form-control" id="clips_number_entry" />
					</div>
					<div class="form-group">
						 
						<label for="coins_entry">
							Coins
						</label>
						<input type="text" class="form-control" id="coins_entry" />
					</div>
					<div class="form-group">
						 
						<label for="color_entry">
							Color
						</label>
						<input type="text" class="form-control" id="color_entry" />
					</div>
					<div style="text-align: center;">
						<button type="button" class="btn btn-default"  onclick="submit_package();">
							Submit
						</button>
					</div>
				</form>
			</div>
			<div class="col-md-8">
				<h3 class="text-center text-primary">
					Packages List
				</h3>
				<table class="table table-condensed table-hover table-bordered">
					<thead>
						<tr >
							<th style="text-align: center;">
								id
							</th>
							<th style="text-align: center;">
								coins
							</th>
							<th style="text-align: center;">
								clips
							</th>
							<th style="text-align: center;">
								color
							</th>
							<th style="text-align: center;">
								status
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>

							</td>
							<td>

							</td>
							<td>

							</td>
							<td>

							</td>
							<td>

							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>

		<hr>

		<div class="row" style="padding:25px;">
			<div class="col-md-12">
				<h3 class="text-center text-primary">
					Clips
				</h3>
				<table class="table table-condensed table-hover table-bordered">
					<thead>
						<tr>
							<th style="text-align: center;">
								id
							</th>
							<th style="text-align: center;">
								clip
							</th>
							<th style="text-align: center;">
								answer
							</th>
							<th style="text-align: center;">
								choices
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>

							</td>
							<td>

							</td>
							<td>

							</td>
							<td>

							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>






    </body>


</html>