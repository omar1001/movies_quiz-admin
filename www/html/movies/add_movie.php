<?php

	if(session_status() != PHP_SESSION_ACTIVE) session_start();

	if(!isset($_SESSION['ADMIN_AUTHENTICATED'])) {
		echo "re-login";
		exit();
	}
	if(!isset($_POST['movie_name'])) {
		echo "enter_movie";
		exit();
	}
	if(strlen($_POST['movie_name']) < 2 || $_POST['movie_name'] == '') die("enter_movie");
	$movie_name = trim($_POST['movie_name']);

	$conn = mysqli_connect('localhost', 'test', '1');
	if($conn === FALSE) {
		echo "database connection";
		exit();
	}
	$movie_name = mysqli_real_escape_string($conn, $movie_name);

	$query = "INSERT INTO movies_quiz.movies (name) VALUES('" . $movie_name . "')";

	mysqli_query($conn, "SET NAMES 'utf8'");

	$result = mysqli_query($conn, $query);
	if($result) {
		echo('done');
	} else {
		echo('duplicate');
	}
?>